-- SUMMARY --

A very simple module to import HTML sites to Drupal.

Import Doc is originally developed to import zip exports from Confluence Wiki to
 Drupal. It accepts a .zip file with HTML pages and files.

The HTML pages are imported to Drupal and an XSL template is required to
 transform the HTML of the pages. Another XSL template is required to extract 
the title from the HTML of the imported pages.
After the import is succesfully complete, the links in the pages 
(linking to either other pages or files provided in the import) are adjusted 
automatically, so link rewriting shouldn't be defined in the content XSL 
template.

A prefix can be entered when importing. The prefix will be prepended to the 
paths of the imported pages. The prefix also defines the folder in the public
 ('public://...') directory where the other (non-HTML) files will be uploaded.

Other options are:

the Menu where menu items (for each imported HTML page) will be entered
content type of the new pages
language for the node (not shown on the screenshot)

-- REQUIREMENTS --

1.php5-xsl
2.php5-tidy
3.php zip support

-- INSTALLATION --

See the installation guide here - http://drupal.org/node/70151
